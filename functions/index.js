// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();
db.settings({ ignoreUndefinedProperties: true })
const runtimeOpts = {
    timeoutSeconds: 540,
}

const getActivityData = require('./getActivityData');
const getGoogleFitData = require('./getGoogleFitData');
const getFitBitData = require('./getFitBitData');
const getGarminData = require('./getGarminData');
const saveAccessToken = require('./saveAccessToken')

const getPostsByUser = require("./getPostsByUser")

// const subscribeFitbit = require("./subscribeFitbit")

exports.getActivityData = functions.https.onRequest(async (req, res) => {
    var callback = await getActivityData.getActivityData(req, res, db);
    res.json({result: callback});
});
exports.getGoogleFitData = functions.https.onRequest(async (req, res) => {
    var callback = await getGoogleFitData.getGoogleFitData(req, res, db);
    res.json({result: callback});
});
exports.saveAccessToken = functions.runWith(runtimeOpts).https.onRequest(async (req, res) => {
    var callback = await saveAccessToken.saveAccessToken(req, res, db);
    res.send(callback)
});
exports.getFitBitData = functions.https.onRequest(async (req, res) => {
    var callback = await getFitBitData.getFitBitData(req, res, db);
    res.json({result: callback});
});
exports.getGarminData = functions.https.onRequest(async (req, res) => {
    var callback = await getGarminData.getGarminData(req, res, db);
    res.json({result: callback});
});
// exports.subscribeFitbit = subscribeFitbit.subscribeFitbit


// exports.getPostsByUser = getPostsByUser.getPostsByUser



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	