const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const admin = require('firebase-admin');


exports.getGarminData = async function(req, res, db) {
    console.log("db ", db)
    const userID = req.body.userID || "1"
    var token = '';
    var activities = []
    var apiActivities = []
    const startDate = req.body.startDate ? new Date(req.body.startDate) : new Date()
    const endDate = req.body.endDate ? new Date(req.body.endDate) : new Date()
    
    var activitiesDbEndDate;
    var apiStartDate;
    // get activities for user 
    const userRef = db.collection('users');
    const activitiesRef = db.collection("activities")
    

    const userDetails = await userRef.where('UserID', '==', userID).get();
    
    
    var LastActivityUpdated;
    if (userDetails.empty) {
        apiStartDate = startDate
        
    } else {
        userDetails.forEach(doc => {
            LastActivityUpdated = doc.data().LastActivityUpdated.toDate()
            token = doc.data().AccessToken
        });
        if (!token) {
            token = req.headers.authorization
        }
        if (moment(endDate) > moment(LastActivityUpdated)) {
            activitiesDbEndDate = LastActivityUpdated
        } else {
            activitiesDbEndDate = endDate
        }
        
        if (moment(startDate) > moment(LastActivityUpdated)) {
            apiStartDate = startDate
        } else {
            if (moment(endDate) > moment(LastActivityUpdated)) {
                apiStartDate = LastActivityUpdated
            } else {
                apiStartDate = endDate
            }
        }
        const activitiesDb = await activitiesRef.where("UserID", "==", userID).where("EndDate", "<=", activitiesDbEndDate).get()
        
        activitiesDb.forEach(doc => {
            if (moment(doc.data().StartDate) > moment(startDate)) {
                activities.push(doc.data())
            } else {

            }
        });
        for (var i=0;i<activities.length;i++) {
            activities[i].StartDate = moment.unix(activities[i].StartDate.seconds).format("MM-DD-YYYY hh:mm:ss")
            activities[i].EndDate = moment.unix(activities[i].EndDate.seconds).format("MM-DD-YYYY hh:mm:ss")
        }
    }
    var callback = {}
    var data = {
        "aggregateBy": 
        [
            {
                "dataTypeName": "com.google.step_count.delta",
            },
      
            {
      
                "dataTypeName": "com.google.calories.expended",
        
            },
            {
                "dataTypeName": "com.google.heart_minutes",
            },
            {
                "dataTypeName": "com.google.distance.delta"
            },
      
        ],
        "bucketByActivitySegment": {
            "minDurationMillis": 180000
        },
        "startTimeMillis": moment(apiStartDate).valueOf(),
        "endTimeMillis": moment(endDate).valueOf()
    }
    await axios.get('https://apis.garmin.com/wellness-api/rest /activities?uploadStartTimeInSeconds=' + moment(apiStartDate).unix() + '&uploadEndTimeInSeconds=' + moment(endDate).unix(), {
        headers: {  
            "Authorization" : token
        }    
    
    })
    .then(function (response) {
        // handle success
        console.log("success for first api")
        var result = response.data
        if (result.bucket && result.bucket.length > 0) {
            for (var i =0;i<result.bucket.length;i++) {
                var obj = {}
                obj.ActivityName = constants[result.bucket[i].activity]
                obj.StartDate = moment(Number(result.bucket[i].startTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                obj.EndDate = moment(Number(result.bucket[i].endTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0) {
                    for (var j=0;j<result.bucket[i].dataset.length;j++) {
                        if (result.bucket[i].dataset[j].point && result.bucket[i].dataset[j].point.length > 0) {
                            if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.calories.expended:com.google.android.gms:aggregated") {
                                // count calories 
                                if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                    obj.CaloriesBurned = result.bucket[i].dataset[j].point[0].value[0].fpVal    
                                }
                            } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.heart_minutes.summary:com.google.android.gms:aggregated") {
                                // count heart points 
                                if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                    obj.HeartPoints = result.bucket[i].dataset[j].point[0].value[0].fpVal
                                    obj.Duration = result.bucket[i].dataset[j].point[0].value[1].intVal/60
                                    obj.ActivityScore = calculateActivityScore(obj.HeartPoints, obj.Duration)
                                }
                            } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.step_count.delta:com.google.android.gms:aggregated") {
                                // count steps
                                if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                    obj.StepCount = result.bucket[i].dataset[j].point[0].value[0].intVal
                                    obj.StepFitPoints = calculateFitPoints(obj.StepCount)
                                }
                            } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.distance.delta:com.google.android.gms:aggregated") {
                                // count distance and average pace
                                if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                    obj.Distance = result.bucket[i].dataset[j].point[0].value[0].fpVal/1000
                                    console.log("Distance ", obj.Distance)
                                    console.log("Duration ", obj.Duration)
                                    if (obj.Duration) {
                                        obj.AveragePace = obj.Distance/obj.Duration
                                    }
                                }  
                            } 
                        }
                        
                    }
                }
                activities.push(obj)
                apiActivities.push(obj)
            }
        } else {
            // no data
        }
        
    })
    .catch(function (error) {
        // handle error
        console.log("Error ", error)
        callback = {
            success: false,
            status: 400,
            error: error,
            request: data,
        }
    })
    console.log("Finished first api ", moment(apiStartDate).valueOf())
    console.log("Finished first api ", moment(endDate).valueOf())
    var stepData = {
        "aggregateBy": [{
            "dataTypeName": "com.google.step_count.delta",
            "dataSourceId": "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
        }],
        "bucketByTime": { "durationMillis": 86400000 },
        "startTimeMillis": moment(apiStartDate).valueOf(),
        "endTimeMillis": moment(endDate).valueOf()
    }
    console.log("Body for second api ", stepData)
    await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',stepData, {
        headers: {
            "Authorization" : token
        }    
    
    })
    .then(function (response){
        var result = response.data
        console.log("step data", result.bucket)
        if (result && result.bucket && result.bucket.length > 0) {
            for (var i=0;i<result.bucket.length;i++) {
                if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0 && result.bucket[i].dataset[0].point && result.bucket[i].dataset[0].point.length > 0) {
                    var obj = {}
                    obj.ActivityName = "Daily Steps"
                    obj.StartDate = moment(Number(result.bucket[i].startTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                    obj.EndDate = moment(Number(result.bucket[i].endTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                    obj.ActivityID = 100
                    if (result.bucket[i].dataset[0].point[0].value && result.bucket[i].dataset[0].point[0].value.length > 0) {
                        obj.StepCount = result.bucket[i].dataset[0].point[0].value[0].intVal
                        obj.StepFitPoints = calculateFitPoints(obj.StepCount)
                    }
                    activities.push(obj)
                    apiActivities.push(obj)
                }
            }
        }
    })
    .catch(function (error){
        console.log("error in second api ", error)
    })
    if (userDetails.empty) {
        await userRef.doc(userID.toString()).set({
            UserID: userID, PrimarySourceID: '1', PrimarySourceName: 'GoogleFit',
            LastActivityUpdated: new Date()
        });
    } else {
        await userRef.doc(userID.toString()).update({
            LastActivityUpdated: new Date()
        });
    }
    callback = {
        success: true,
        status: 200,
        data: {
            userID: userID,
            sourceCode: 1,
            sourceName: "GoogleFit",
            activities: activities
        }
    }
    console.log("end this here ", apiActivities.length)
    for (i=0;i<apiActivities.length;i++) {
        
        await activitiesRef.doc().set({
            UserID: userID, 
            ActivityName: apiActivities[i].ActivityName, 
            StartDate: apiActivities[i].StartDate ? new Date(apiActivities[i].StartDate) : undefined,
            EndDate: apiActivities[i].EndDate ? new Date(apiActivities[i].EndDate) : undefined,
            Duration: apiActivities[i].Duration,
            CaloriesBurned: apiActivities[i].CaloriesBurned,
            HeartPoints: apiActivities[i].HeartPoints,
            ActivityScore: apiActivities[i].ActivityScore,
            StepCount: apiActivities[i].StepCount,
            StepFitPoints: apiActivities[i].StepFitPoints,
            Distance: apiActivities[i].Distance,
            AveragePace: apiActivities[i].AveragePace
        });
    }
    return callback
    // Send back a message that we've successfully written the message
    
};


var calculateFitPoints = function (steps) {
    return steps * 0.002
    // var score = 0;
    // if (steps < 2500) {
    //     score = 0 
    // } else if (steps >=2500 && steps < 5000) {
    //     score = 0.6 * ((steps - 2500)/ 500)
    // } else if (steps >= 5000 && steps < 7500) {
    //   score = 0.6 * ((steps - 2500)/ 500)
    // } else if (steps >= 7500 && steps < 10000) {
    //     score = 0.6 * 30
    // } else if (steps >= 10000 && steps < 12500) {
    //     score = 0.6 * (30 + ((steps - 10000)/ 500))
    // } else {
    //   score = 0.6 * (35 + ((steps - 12500)/ 500))
    // }
    // return score
}

var calculateActivityScore = function (heartPoints, duration) {
    var activityScore = 0
    if (heartPoints > duration) {
        activityScore = heartPoints
    } else {

    }
    return activityScore
}
