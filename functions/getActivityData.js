const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const _ = require("lodash")

// const admin = require('firebase-admin');
// admin.initializeApp();
// const db = admin.firestore();

const getGoogleFitData = require('./getGoogleFitData');
const getFitBitData = require('./getFitBitData');
const getGarminData = require('./getGarminData');

exports.getActivityData = async function (req, res, db) {
    console.log("inside get activity data")
    var sourceID = req.body.sourceID
    var activitiesObject = {}
    var callback = {}
    var activities = []
    var body = {
        userID: userID,
        startDate: startDate,
        endDate: endDate
    }
    
    if (sourceID == 1) {
        console.log("Making a call to google fit.", token)
        // get google fit data
        callback = await getGoogleFitData.getGoogleFitData(req, res, db);
    } else if (sourceID == 2) {
        // get fitbit data 
        callback = await getFitBitData.getFitBitData(req, res, db);
    } else if (sourceID == 3) {
        callback = await getGarminData.getGarminData(req,res,db)
    }
    return callback
    
};