const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const admin = require('firebase-admin');
const _ = require("lodash")


exports.getGoogleFitData = async function(req, res, db) {
    console.log("db ", db)
    const userID = req.body.userID || "1"
    // var token = '';
    var activities = []
    var calendar = []
    var apiActivities = []
    const startDate = req.body.startDate ? new Date(req.body.startDate) : new Date()
    const endDate = req.body.endDate ? new Date(req.body.endDate) : new Date()
    var days = []
    var weeklyData = {
        numberOfActivities : 0,
        fitScore: 0,
        minutes: 0,
        steps: 0,
        calories: 0
    }
    var monthlyData = {
        numberOfActivities : 0,
        fitScore: 0,
        minutes: 0,
        steps: 0,
        calories: 0
    }
    
    var activitiesDbEndDate;
    var apiStartDate;
    // get activities for user 
    const userRef = db.collection('users');
    const activitiesRef = db.collection("activities")
    const calendarRef = db.collection("calendar")
    var totalNumberOfActivities = 0
    

    const userDetails = await userRef.where('UserID', '==', userID).get();
    const calendarDetails = await calendarRef.where("UserID", "==", userID).get();
    
    console.log("here 1")
    var LastActivityUpdated;
    if (userDetails.empty) {
        apiStartDate = startDate
        totalNumberOfActivities = 0
        
    } else {
        console.log("here 2")
        userDetails.forEach(doc => {
            LastActivityUpdated = doc.data().LastActivityUpdated.toDate()
            totalNumberOfActivities = doc.data().TotalNumberOfActivities
            // token = doc.data().AccessToken
        });
        if (moment(endDate) > moment(LastActivityUpdated)) {
            activitiesDbEndDate = LastActivityUpdated
        } else {
            activitiesDbEndDate = endDate
        }
        
        if (moment(startDate) > moment(LastActivityUpdated)) {
            apiStartDate = startDate
        } else {
            if (moment(endDate) > moment(LastActivityUpdated)) {
                apiStartDate = LastActivityUpdated
            } else {
                apiStartDate = endDate
            }
        }
        const activitiesDb = await activitiesRef.where("UserID", "==", userID).where("EndDate", "<=", activitiesDbEndDate).get()
        
        activitiesDb.forEach(doc => {
            if (moment(doc.data().StartDate) > moment(startDate)) {
                activities.push(doc.data())
            } else {

            }
        });
        
        for (var i=0;i<activities.length;i++) {
            activities[i].StartDate = moment.unix(activities[i].StartDate.seconds).format("MM-DD-YYYY hh:mm:ss")
            activities[i].EndDate = moment.unix(activities[i].EndDate.seconds).format("MM-DD-YYYY hh:mm:ss")
        }
        calendarDetails.forEach(doc => {
            var obj = {}
            var todayActivityArray = []
            for (var i=0;i<doc.data().ActivityIDs.length;i++) {
                var todayActivityObject = _.find(activities, function(o) {
                    return o.ActivityID = doc.data().ActivityIDs[i]
                })
                todayActivityArray.push(todayActivityObject)
                if (moment.unix(doc.data().StartDate._seconds).isBefore(moment()) && moment.unix(doc.data().StartDate._seconds).isAfter(moment().startOf("week"))) {
                    console.log("Inside this week ", moment(doc.data().StartDate).format("MM-DD-YYYY"))
                    weeklyData.numberOfActivities ++
                    weeklyData.fitScore = weeklyData.fitScore + parseFloat(todayActivityObject.StepFitPoints)
                    if (todayActivityObject.Duration) {
                        weeklyData.minutes = weeklyData.minutes + parseFloat(todayActivityObject.Duration)
                    }
                    weeklyData.steps = weeklyData.steps + parseFloat(todayActivityObject.StepCount)
                    weeklyData.calories = weeklyData.calories + parseFloat(todayActivityObject.CaloriesBurned)
                }
                if (moment.unix(doc.data().StartDate._seconds).isBefore(moment()) && moment.unix(doc.data().StartDate._seconds).isAfter(moment().startOf("month"))) {
                    console.log("Inside this month ", moment(doc.data().StartDate).format("MM-DD-YYYY"))
                    monthlyData.numberOfActivities ++
                    monthlyData.fitScore = monthlyData.fitScore + parseFloat(todayActivityObject.StepFitPoints)
                    if (todayActivityObject.Duration) {
                        monthlyData.minutes = monthlyData.minutes + parseFloat(todayActivityObject.Duration)
                    }
                    monthlyData.steps = monthlyData.steps + parseFloat(todayActivityObject.StepCount)
                    monthlyData.calories = monthlyData.calories + parseFloat(todayActivityObject.CaloriesBurned)
                }
            }
            obj.ActivityDate = moment(doc.data().StartDate).format("MM-DD-YYYY")
            obj.dailyFitScore = doc.data().DailyFitScore
            obj.activitiesForDay = todayActivityArray
            days.push(obj)
        })

    }
    var callback = {}
    
    
    callback = {
        success: true,
        status: 200,
        data: {
            userID: userID,
            sourceCode: 1,
            sourceName: "GoogleFit",
            Days: days,
            weeklyData: weeklyData,
            monthlyData: monthlyData
        }
    }
    
    return callback
    // Send back a message that we've successfully written the message
    
};


